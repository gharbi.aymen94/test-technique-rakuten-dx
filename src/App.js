import React, { Component } from "react";
import { Platform, View, StatusBar } from "react-native";
import { Modal } from "@components";

// Utils
import { Provider } from "react-redux";
import Navigator from "./navigator";
import store from "./store";
import { NavigationService } from "@services";

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <StatusBar
                    backgroundColor="black"
                    barStyle={
                        Platform.OS === "ios" ? "dark-content" : "light-content"
                    }
                />
                <View style={{ flex: 1 }}>
                    <Modal />
                    <Navigator
                        ref={navigatorRef => {
                            NavigationService.setTopLevelNavigator(
                                navigatorRef
                            );
                        }}
                    />
                </View>
            </Provider>
        );
    }
}
