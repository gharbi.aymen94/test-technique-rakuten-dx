export default {
    bold: "AvenirLTStd-Black",
    medium: "AvenirLTStd-Medium",
    regular: "AvenirLTStd-Roman",
    light: "AvenirLTStd-Light"
};
